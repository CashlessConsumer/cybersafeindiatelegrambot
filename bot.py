"""
Simple Bot to reply to Telegram messages taken from the python-telegram-bot examples.
Deployed using heroku.
Author: liuhh02 https://medium.com/@liuhh02
"""
import json
import logging
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import os
import html2text
import requests


PORT = int(os.environ.get('PORT', 5000))

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)

logger = logging.getLogger(__name__)
TOKEN = os.environ.get('TELEGRAM_BOT_TOKEN')


def queryMobile(mobno):
    if(len(mobno) != 10):
        return 'Enter valid phone number'
    payload = json.dumps({
    "mob_acc": mobno,
    "data_type": "mobile_no",
    "source": "landing_page"
    })
    http_proxy  = "http://proxy.sarkar.icu:8888"
    https_proxy = "https://proxy.sarkar.icu:8888"
    proxyDict = { 
              "http"  : http_proxy,
              "https" : https_proxy
            }

    headers = {
    'Host': 'cybersafe.gov.in',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0',
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
    }
    logging.debug('Before querying CyberSafe')
    response = requests.request("POST", os.environ.get('CYBERSAFE_URL'), headers=headers, data=payload, proxies=proxyDict, verify=False)
    logging.debug('Response from  CyberSafe' + response.text)
    return (html2text.html2text(response.text))

# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')

def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')

def echo(update, context):
    """Echo the user message."""
    update.message.reply_text(update.message.text)

def verifyMobile(update, context):
    """Check the mobile number's existance in cybersafe database."""
    update.message.reply_text(queryMobile(update.message.text.split(' ')[1]))

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)

def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("verifyMobile", verifyMobile))
    

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_webhook(listen="0.0.0.0",
                          port=int(PORT),
                          url_path=TOKEN)
    updater.bot.setWebhook('https://cybersafeindiacc.herokuapp.com/' + TOKEN)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()
